/* Array representation : adjacency list, adjacency matrix */

#include <iostream>
#include <iomanip>
#include <ctime>

#define MSIZE 4
using namespace std;

struct AdjList;
void print_matrix(const int (*arr)[MSIZE]);
void generate_random_matrix();
AdjList *createNode(const int gnode_id);

typedef struct AdjList {
    int node_id;
    AdjList *nxtlink;
} * pAdjList;

void print_matrix(const int (*arr)[MSIZE]) {
    for (int i = 0; i < MSIZE; ++i) {
        for (int j = 0; j < MSIZE; ++j) {
            cout << arr[i][j] << "\t";
        }
        cout << endl;
    }
}

AdjList *createNode(const int gnode_id) {
    pAdjList p = new AdjList;
    p->node_id = gnode_id;
    p->nxtlink = nullptr;
    return p;
}

int arr_mat[MSIZE][MSIZE] = {};

void generate_random_matrix() {
    int ranvalue;
    srand (time(NULL));
    for (int i = 0; i < MSIZE; ++i) {
        for (int j = 0; j < MSIZE; ++j) {
            ranvalue = rand() % 2;
            arr_mat[i][j] = ranvalue;
        }
    }
    return;
}

int main() {
    generate_random_matrix();
    pAdjList arlst[MSIZE] = {}; // array of list
    pAdjList new_node = new AdjList;
    pAdjList startnode = new AdjList; /* for iteration */
    pAdjList lastnode = new AdjList; /* for insert */
    bool isnode1set = false;
    print_matrix(arr_mat);

    for (int i = 0; i < MSIZE; ++i) {
        for (int j = 0; j < MSIZE; ++j) {
            if (arr_mat[i][j] == 1) {
                new_node = createNode(j);
                if (isnode1set == false)
                {
                    arlst[i] = new_node;
                    startnode = arlst[i];
                    lastnode = startnode;
                    isnode1set = true;
                }
                else
                {
                    lastnode->nxtlink = new_node;
                    lastnode = lastnode->nxtlink;
                }
            }
        }
        isnode1set = false;
    }

    cout << "Array : List of nodes\n\n";
    for (int k = 0; k < MSIZE; ++k) {
        startnode = arlst[k];
        cout << setw(6) << k << ":"; // prints index

        if (startnode == nullptr) {   // when startnode is 0x0 i.e when matrix is {0,0,0}, leads to SEGFAULT
            cout << "x"; // print anything to represent NULL
        }
        else {
            while (startnode->nxtlink != nullptr) {
                cout << startnode->node_id << char(26);
                startnode = startnode->nxtlink;
            }
            cout << startnode->node_id; // print last node
        }
        cout << "\n";
    }
    return 0;
}

/*
if (a[][] == 1)
    then: create new node

else if (a[][] == 0)
    then: None
*/
