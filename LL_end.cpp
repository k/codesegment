// Linked list  - insertion at end, deletion(non-start/non-rear element)
#include <cstdio>

typedef struct Node {
    size_t info;
    Node *nextnode;
}*pNode;

pNode start, rear, newptr, ptr, save;

Node* CreateNodeEnd(size_t data);
void InsertNodeEnd(pNode np);
void DisplayEnd(pNode np);
void generate_lst();
Node* searchElement(pNode np , size_t elem_find);
int DeleteElement(pNode ElementinList);

Node *CreateNodeEnd(int value) {
    ptr = new Node;
    ptr->info = value;
    ptr->nextnode = nullptr;
    return ptr;
}

void InsertNodeEnd(pNode np) {  // insertion at rear-end
    if (start == nullptr) {
        start = np;
        rear = np;
    } else {
        rear->nextnode = np;
        rear = np; // rear->nextnode = nullptr; no need as already done above while creating node
    }
}

void displayNodes(pNode np) {
    while (np != nullptr) {
        printf("[%#x [%d::%#x]]    ",np, np->info, np->nextnode); /* [node-address [data|link]] */
        np = np->nextnode;
    }
    printf("%c\n", char(176));
    return;
}

Node *searchElement(pNode np , size_t elem_find) {
    int i = 0;
    while (np != nullptr) {
        if (np->info == elem_find)
        {
            printf("Element found at location: %d, at node = %#x\n", i, np );
            return np;
        }
        else{
            np = np->nextnode; ++i;
        }
    }
    return (np);	// return (np = nullptr);
}

int DeleteElement(pNode ElementinList) {
    save = ElementinList;	// ElementinList =  will point to node to be delete
    printf("--------------------------------------\n");
    printf("ElementinList: %#x\n",ElementinList);
    printf("ElementinList->info: %d\n", ElementinList->info);
    printf("ElementinList->nextnode: %#x \n", ElementinList->nextnode);
    printf("save: %#x\n", save);
    printf("save->nextnode->nextnode: %#x\n", save->nextnode->nextnode);
    printf("--------------------------------------\n");
    pNode traverse = start;
    while(traverse->nextnode!=nullptr )
    {
        if(traverse->nextnode == save)		// check if node->nextnode has address of = node to be deleted. Instead to get previous node's(i.e to delete) address
        {
            traverse->nextnode = save->nextnode;
            delete ElementinList;
        }
        traverse = traverse->nextnode; // incr next node
    }
    delete save;
    return 0;
}

void generate_lst() {
    size_t element = 0;
    start = nullptr;
    rear = nullptr;
    char ch = 'y';
    int data;
    do {
        printf("Enter value(int): "); scanf("%d",&data);
        newptr = CreateNodeEnd(data);
        InsertNodeEnd(newptr);
        displayNodes(start);
        printf("want to enter more(y/n)?: "); scanf("%s", &ch);

    } while (ch == 'Y' || ch == 'y');
    printf("Enter (non-start/non-end)element to delete: ");
    scanf("%d", &element);
    Node *elementExist = searchElement(start, element);
    if (elementExist == nullptr)
        printf("Element does not exist!!");
    else {
        DeleteElement(elementExist);
        displayNodes(start);
    }
    return;
}

int main(/*int argc, char *argv[]*/) {
    generate_lst();
    return 0;
}

/*
 * Enter Data: 10
10->0 | !!
Enter [Y/n]: y
Enter Data: 20
10->0x66b530 | 20->0 | !!
Enter [Y/n]: y
Enter Data: 30
10->0x66b530 | 20->0x66b540 | 30->0 | !!
Enter [Y/n]: y
Enter Data: 40
10->0x66b530 | 20->0x66b540 | 30->0x66b550 | 40->0 | !!
Enter [Y/n]: y
Enter Data: 50
10->0x66b530 | 20->0x66b540 | 30->0x66b550 | 40->0x66b560 | 50->0 | !!
Enter [Y/n]: m
Enter element to delete: 30
Element at location: 2 np = 0x66b540
10->0x66b530 | 20->0x66b540 | 30->0x66b550 | 40->0x66b560 | 50->0 | !!
*/

/*
 * Enter Data: 50
[0x34b600 [10[link:0x34b610]] ] || [0x34b610 [20[link:0x34b620]] ] || [0x34b620 [30[link:0x34b630]] ] || [0x34b630 [40[link:0x34b640]] ] || [0x34b640 [50[link:0]] ] || !!
Enter [Y/n]: r
Enter element to delete: 30
Element at location: 2 np = 0x34b620
--------------------------------------
ElementinList: 0x34b620
ElementinList->info: 30
ElementinList->nextnode: 0x34b630
save: 0x34b620
save->nextnode->nextnode: 0x34b640
--------------------------------------
[0x34b600 [10[link:0x34b610]] ] || [0x34b610 [20[link:0x34b620]] ] || [0x34b620 [30[link:0x34b640]] ] || [0x34b640 [50[link:0]] ] || !!
 */
