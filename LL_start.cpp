// Linked list  - insertion at start
#include <cstdio>

typedef struct Node {
    size_t info;
    Node *nextnode;
}*pNode;

pNode start, newptr, ptr, save;

Node *CreateNode(size_t data);
int InsertNodeBegin(pNode np);
void displayNodes(pNode np);
void generate_lst();

Node *CreateNode(size_t data) {
    ptr = new Node;
    ptr->info = data;
    ptr->nextnode = nullptr;
    return ptr;
}

int InsertNodeBegin(pNode np) { // insert node at begining
    if (start == nullptr)
        start = np;
    else {
        save = start;
        start = np;
        np->nextnode = save;
    }
    return 0;
}

void displayNodes(pNode np) {
    while (np != nullptr) {
        printf("[%#x [%d::%#x]]    ",np, np->info, np->nextnode); /* [node-address [data|link]] */
        np = np->nextnode;
    }
    printf("!!\n");
    return;
}

void generate_lst() {
    char ch = 'y';
    start = nullptr;
    size_t data = 0;
    do {
        printf("Enter value(int): "); scanf("%d",&data);
        newptr = CreateNode(data);
        InsertNodeBegin(newptr);
        displayNodes(start);
        printf("want to enter more(y/n)?: "); scanf("%s", &ch);
    } while (ch == 'y' || ch == 'Y');
    return;
}

int main(/*int argc, char *argv[]*/) {
    generate_lst();
    return 0;
}
